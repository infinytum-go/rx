package rx

import "github.com/google/uuid"

// Subscription represents a string to identify
// a subscription in an obserable so it can be removed
type Subscription uuid.UUID

// NewSubscription generates a new subscription
func NewSubscription() Subscription {
	return Subscription(uuid.New())
}

// EmptySubscription returns a Nil value for subscription
func EmptySubscription() Subscription {
	return Subscription(uuid.Nil)
}
