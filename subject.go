package rx

import (
	"errors"
	"reflect"
	"sync"
)

// subject is the basic implementation of a subjectable
type subject struct {
	Subscriptions map[Subscription]interface{}
	sync.Mutex
}

// AsChannel returns a channel which will receive all
// further updates of this observable
func (s *subject) AsChannel() chan []interface{} {
	defer s.Unlock()
	s.Lock()
	channel := make(chan []interface{})
	go s.Subscribe(func(args ...interface{}) {
		go func(channel chan []interface{}) {
			channel <- args
		}(channel)
	})
	return channel
}

// Close will remove all subscribers and render the subjectable useless
func (s *subject) Close() {
	defer s.Unlock()
	s.Lock()
	s.Subscriptions = make(map[Subscription]interface{})
}

// Next takes an undefined amount of parameters which will be passed to
// subscribed functions
func (s *subject) Next(values ...interface{}) {
	defer s.Unlock()
	s.Lock()
	for subscription := range s.Subscriptions {
		s.notifySubscriber(subscription, values)
	}
}

func (s subject) notifySubscriber(subscription Subscription, values []interface{}) {
	if fn, ok := s.Subscriptions[subscription]; ok {
		refFn := reflect.TypeOf(fn)
		fnArgs := make([]reflect.Value, 0, refFn.NumIn())

		for argIndex := 0; argIndex < refFn.NumIn(); argIndex++ {
			// Skipping subscribers which have more arguments than this array provides
			if len(values) == argIndex {
				return
			}

			providedVal := values[argIndex]

			// Variadic arguments need special treatment
			if refFn.IsVariadic() && refFn.In(argIndex).Kind() == reflect.Slice && argIndex == refFn.NumIn()-1 {
				sliceType := refFn.In(argIndex).Elem()

				for _, innerVal := range values[argIndex:len(values)] {
					// Pass empty slice for variadic if nothing was passed for it
					if innerVal == nil {
						fnArgs = append(fnArgs, reflect.New(sliceType).Elem())
						continue
					}

					// Check if the innerValue matches the expected type.
					if !reflect.TypeOf(innerVal).AssignableTo(sliceType) {
						// Slice does not match received data, skipping this subscriber
						return
					}
					fnArgs = append(fnArgs, reflect.ValueOf(innerVal))
				}
				// Finish loop as we have filled in all data to the slice
				break
			} else {
				argType := refFn.In(argIndex)
				// Create zero value for input if nil was given
				if providedVal == nil {
					values[argIndex] = reflect.New(argType).Elem()
					providedVal = values[argIndex]
				}

				// Check if argument is compatible with expected type
				if !reflect.TypeOf(providedVal).AssignableTo(argType) {
					// Method signature not compatible with this input. Skipping subscriber
					return
				}

				fnArgs = append(fnArgs, reflect.ValueOf(values[argIndex]))

				// Check if we have reached the end of the maximum function capacity
				if argIndex == refFn.NumIn()-1 {
					// Check if the amount of args matches the expected amount of params
					if refFn.NumIn() != len(fnArgs) {
						// Skipping func if the amount missmatches
						return
					}
				}
			}

		}

		reflect.ValueOf(fn).Call(fnArgs)
	}
}

// Pipe decorates an observable with one or multiple middlewares
// and returns a new observable with the decoration applied
func (s *subject) Pipe(fns ...func(Observable, Subjectable)) Observable {
	parent := s
	for _, fn := range fns {
		if fn == nil {
			continue
		}
		sub := NewSubject().(*subject)
		fn(parent, sub)
		parent = sub
	}
	return parent
}

// Subscribe registers a function for further updates of
// this observable and returns a subscription token which can
// be used to unsubscribe from it at any time
func (s *subject) Subscribe(fn interface{}) (Subscription, error) {
	defer s.Unlock()
	s.Lock()
	if fn != nil && reflect.TypeOf(fn).Kind() == reflect.Func {
		subscription := NewSubscription()
		s.Subscriptions[subscription] = fn

		return subscription, nil
	}
	return EmptySubscription(), errors.New("fn is not a function")
}

// Unsubscribe unregisters a previously registered function for all
// further updates of this observable or until re-registering.
func (s *subject) Unsubscribe(subscription Subscription) error {
	defer s.Unlock()
	s.Lock()
	if _, ok := s.Subscriptions[subscription]; !ok {
		return errors.New("Subscription not found in subject")
	}
	delete(s.Subscriptions, subscription)
	return nil
}

// NewSubject returns a pointer
// to an empty instance of subject
func NewSubject() Subjectable {
	return &subject{
		Subscriptions: make(map[Subscription]interface{}),
	}
}
