# RxGo [![GoDoc](https://godoc.org/gitlab.com/infinytum-go/rx?status.svg)](https://godoc.org/gitlab.com/infinytum-go/rx) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/infinytum-go/rx)](https://goreportcard.com/report/gitlab.com/infinytum-go/rx) [![Codacy Badge](https://api.codacy.com/project/badge/Grade/e7de2c3854bd42babfd89f75cb78ab00)](https://www.codacy.com/app/infinytum/reactive?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=infinytum/reactive&amp;utm_campaign=Badge_Grade) [![coverage report](https://gitlab.com/infinytum-go/rx/badges/master/coverage.svg)](https://gitlab.com/infinytum-go/rx/commits/master) [![pipeline status](https://gitlab.com/infinytum-go/rx/badges/master/pipeline.svg)](https://gitlab.com/infinytum-go/rx/commits/master)
My attempt on creating a simple RxJs clone

## Features
*  Observables
   *  Multi-Type support
*  Subjects
   *  Subject
   *  ReplaySubject
*  Pipes
   *  Take
   *  TakeEvery
   *  Skip
   *  SkipEvery

## Examples

### Simple Subject

```go
package main

import (
    "gitlab.com/infinytum-go/rx"
    "fmt"
)

func main() {
	subject := rx.NewSubject()
	subject.Subscribe(subHandler)
	subject.Next(1)
	subject.Next(2)
	subject.Next(3)
	subject.Next(4)
}

func subHandler(a int) {
	fmt.Println(a)
}
```

Output
```
$ go run main.go
1
2
3
4
```

### Replay Subject

```go
package main

import (
    "gitlab.com/infinytum-go/rx"
    "fmt"
)

func main() {
    subject := rx.NewReplaySubject()
    subject.Next(1)
    subject.Next(2)
    subject.Next(3)
    subject.Subscribe(subHandler)
    subject.Next(4)
}

func subHandler(a int) {
	fmt.Println(a)
}
```

Output
```
$ go run main.go
3
4
```

### Multi-Type support

```go
package main

import (
    "gitlab.com/infinytum-go/rx"
    "fmt"
)

func main() {
	subject := rx.NewSubject()

	subject.Subscribe(intHandler)
	subject.Subscribe(stringHandler)

	subject.Next(2)
	subject.Next("Hello")
	subject.Next("World")
	subject.Next(4)
	subject.Next(nil)
}

func intHandler(a int) {
	fmt.Print("Int Handler: ")
	fmt.Println(a)
}

func stringHandler(a string) {
	fmt.Print("String Handler: ")
	fmt.Println(a)
}
```

Output
```
Int Handler: 2
String Handler: Hello
String Handler: World
Int Handler: 4
Int Handler: 0
String Handler:
```

### Take Pipe

```go
package main

import (
    "gitlab.com/infinytum-go/rx"
    "fmt"
)

func main() {
    subject := rx.NewReplaySubject()
    subject.Pipe(rx.Take(2)).Subscribe(subHandler)
    subject.Next(1)
    subject.Next(2)
    subject.Next(3)
    subject.Next(4)
}

func subHandler(a int) {
	fmt.Println(a)
}
```

Output
```
$ go run main.go
1
2
```

### TakeEvery Pipe

```go
package main

import (
    "gitlab.com/infinytum-go/rx"
    "fmt"
)

func main() {
    subject := rx.NewReplaySubject()
    subject.Pipe(rx.TakeEvery(2)).Subscribe(subHandler)
    subject.Next(1)
    subject.Next(2)
    subject.Next(3)
    subject.Next(4)
}

func subHandler(a int) {
	fmt.Println(a)
}
```

Output
```
$ go run main.go
2
4
```

### Skip Pipe

```go
package main

import (
    "gitlab.com/infinytum-go/rx"
    "fmt"
)

func main() {
    subject := rx.NewReplaySubject()
    subject.Pipe(rx.Skip(2)).Subscribe(subHandler)
    subject.Next(1)
    subject.Next(2)
    subject.Next(3)
    subject.Next(4)
}

func subHandler(a int) {
	fmt.Println(a)
}
```

Output
```
$ go run main.go
3
4
```

### SkipEvery Pipe

```go
package main

import (
    "gitlab.com/infinytum-go/rx"
    "fmt"
)

func main() {
    subject := rx.NewReplaySubject()
    subject.Pipe(rx.SkipEvery(2)).Subscribe(subHandler)
    subject.Next(1)
    subject.Next(2)
    subject.Next(3)
    subject.Next(4)
}

func subHandler(a int) {
	fmt.Println(a)
}
```

Output
```
$ go run main.go
1
3
```
